// Package utils provides ...
package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"

	"github.com/danielcherubini/npm-migrate/models"
)

// HandleError takes an error object and exits
func HandleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// SingleDir takes a directory String and returns a Dir struct
func SingleDir(dir string) []models.Dir {
	var dirs []models.Dir
	dirs = append(dirs, models.Dir{Path: dir})

	return dirs
}

// WalkDir takes a directory String and returns a Dir struct array
func WalkDir(dir string) ([]models.Dir, error) {
	var dirs []models.Dir

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return dirs, err
	}

	for _, v := range files {
		thisDir := models.Dir{}
		joinedDir := dir + "/" + v.Name()

		thisDir.Path = joinedDir
		dirs = append(dirs, thisDir)
	}

	return dirs, nil
}

// GetPublishedPackages takes an NPMPackage String and returns a string array
func GetPublishedPackages(dir models.Dir) []string {
	var versions []string
	pjs, _ := LoadPackageJSON(dir.Path)
	cmd := exec.Command("npm", "info", "--json", pjs.Name, "versions")
	out, _ := cmd.Output()

	_ = json.Unmarshal(out, &versions)

	return versions
}

// LoadPackageJSON takes a path and returns a PackageJSON Struct
func LoadPackageJSON(path string) (models.PackageJSON, error) {
	var pjs models.PackageJSON
	file, err := ioutil.ReadFile(path + "/package.json") // for read access.
	if err != nil {
		return pjs, err
	}
	json.Unmarshal(file, &pjs)
	return pjs, nil
}

func checkNPMrc(job models.Job) error {
	file, _ := ioutil.ReadFile(job.Path + "/.npmrc")
	if file != nil {
		npmrc := []byte(job.NPMrc)
		err := ioutil.WriteFile(job.Path+"/.npmrc", npmrc, 0644)
		if err != nil {
			return err
		}
	}
	return nil
}

// NPMInstall runs NPM Install
func NPMInstall(job models.Job) error {
	if job.Verbose {
		fmt.Println("Running Install")
	}
	cmd := exec.Command("npm", "install", "--no-package-lock")
	cmd.Dir = job.Path
	out, err := cmd.CombinedOutput()
	if err != nil {
		if job.Verbose {
			fmt.Println(string(out))
		}
		return err
	}

	return nil
}

// NPMPublish takes a Job Struct and publishes the package to NPM
func NPMPublish(job models.Job) error {
	if err := checkNPMrc(job); err != nil {
		return err
	}
	pjs, _ := LoadPackageJSON(job.Path)

	if job.Install || pjs.Scripts.RequiresBuild() {
		if err := NPMInstall(job); err != nil {
			if job.Verbose {
				fmt.Println("Error Installing")
				return err
			}
		}
	}
	if job.Verbose {
		fmt.Println("starting publish")
	}
	cmd := exec.Command("npm", "publish", "--registry", job.Registry)
	cmd.Dir = job.Path
	out, err := cmd.CombinedOutput()
	if err != nil {
		if job.Verbose {
			fmt.Println(string(out))
		}
		return err
	}

	if job.Verbose {
		fmt.Printf("Published %s @ %s\n", pjs.Name, pjs.Version)
	}

	return nil
}
