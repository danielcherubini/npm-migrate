// Package  provides ...
package main

import (
	"flag"
	"fmt"
	"sort"

	"github.com/danielcherubini/npm-migrate/git"
	"github.com/danielcherubini/npm-migrate/models"
	"github.com/danielcherubini/npm-migrate/utils"
	"github.com/schollz/progressbar"
)

func doJobs(job models.Job) {
	if !job.Done {
		err := git.CheckoutToHash(job)
		utils.HandleError(err)
		err = utils.NPMPublish(job)
		// utils.HandleError(err)
		job.Done = true
	}
}

func main() {
	var dir string
	var username string
	var password string
	var registry string
	var npmrc string
	var single bool
	var install bool
	var verbose bool
	var jobs []models.Job
	var dirs []models.Dir
	var err error
	var bar *progressbar.ProgressBar

	flag.StringVar(&dir, "dir", "", "a directory you want to walk")
	flag.StringVar(&username, "username", "", "a username for git")
	flag.StringVar(&password, "password", "", "a password for git")
	flag.StringVar(&registry, "registry", "", "a registry to push too")
	flag.StringVar(&npmrc, "npmrc", "", "the contents of npmrc")
	flag.BoolVar(&single, "single", false, "run on a single dir only")
	flag.BoolVar(&install, "install", false, "run npm install")
	flag.BoolVar(&verbose, "v", false, "verbose")
	flag.Parse()

	if single {
		dirs = utils.SingleDir(dir)
	} else {
		dirs, err = utils.WalkDir(dir)
		utils.HandleError(err)
	}

	for _, dir := range dirs {
		dir, err := git.Reset(dir, username, password)
		utils.HandleError(err)
		dir, err = git.GetTags(dir)
		utils.HandleError(err)
		jobs, err = git.EachTag(dir, jobs, registry, install, npmrc, verbose)
		utils.HandleError(err)
	}

	var finalJobs []models.Job

	for _, job := range jobs {
		for _, published := range job.AlreadyPublished {
			if job.Tag.Name != "v"+published {
				fmt.Println(job.Tag.Name)
				break
				// finalJobs = append(finalJobs, job)
			}
		}
	}

	sort.Slice(jobs, func(i int, j int) bool { return finalJobs[i].Path < finalJobs[j].Path })

	fmt.Println("-----------")
	fmt.Printf("Starting %d Jobs\n", len(finalJobs))

	if !verbose {
		bar = progressbar.NewOptions(len(finalJobs), progressbar.OptionSetRenderBlankState(true))
	}
	for _, job := range finalJobs {
		doJobs(job)
		if !verbose {
			bar.Add(1)
		}
	}

	fmt.Println("-----------")
	fmt.Println("Pushing latest")
	for _, dir := range dirs {
		dir, err := git.Reset(dir, username, password)
		utils.HandleError(err)

		lastRun := models.Job{
			Path:        dir.Path,
			PackageJSON: dir.PackageJSON,
			Registry:    registry,
			Install:     install,
			NPMrc:       npmrc,
			Verbose:     verbose,
		}

		err = utils.NPMPublish(lastRun)
		utils.HandleError(err)
	}
}
