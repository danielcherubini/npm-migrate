// Package git provides ...
package git

import (
	"fmt"

	"github.com/danielcherubini/npm-migrate/models"
	"github.com/danielcherubini/npm-migrate/utils"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
)

// Reset takes a Dir struct and a username and password for git, it returns a Dir struct
func Reset(dir models.Dir, username, password string) (models.Dir, error) {
	r, err := git.PlainOpen(dir.Path)
	if err != nil {
		return dir, err
	}

	auth := &http.BasicAuth{
		Username: username,
		Password: password,
	}

	dir.Repo = r
	fmt.Printf("\n---------------\n")
	fmt.Println("Fetching ", dir.Path)

	err = r.Fetch(&git.FetchOptions{
		RemoteName: "origin",
		Auth:       auth,
		Force:      true,
		Tags:       git.AllTags,
	})
	if err != nil && err != git.NoErrAlreadyUpToDate {
		return dir, err
	}

	head, err := r.Head()
	if err != nil {
		return dir, err
	}

	w, err := r.Worktree()
	if err != nil {
		return dir, err

	}
	fmt.Println("Resetting to HEAD")
	err = w.Reset(&git.ResetOptions{
		Commit: head.Hash(),
		Mode:   git.HardReset,
	})

	fmt.Println("Checking out to origin/master")
	err = w.Checkout(&git.CheckoutOptions{
		Hash: plumbing.NewHash("origin/master"),
	})
	if err != nil {
		return dir, err
	}

	pjs, err := utils.LoadPackageJSON(dir.Path)
	if err != nil {
		return dir, err
	}
	dir.PackageJSON = pjs

	return dir, nil
}

// GetTags takes a Dir struct
func GetTags(dir models.Dir) (models.Dir, error) {
	tags, err := dir.Repo.TagObjects()
	if err != nil {
		return dir, err
	}

	err = tags.ForEach(func(t *object.Tag) error {
		if t.Type() == plumbing.TagObject {
			dir.Tags = append(dir.Tags, t)
		}
		return nil
	})
	if err != nil {
		return dir, err
	}

	return dir, nil
}

// EachTag takes a Dir struct a Job Struct array, and a registry string, returns a Job array
func EachTag(dir models.Dir, jobs []models.Job, registry string, install bool, npmrc string, verbose bool) ([]models.Job, error) {
	fmt.Printf("Iterating through %d tags\n", len(dir.Tags))
	pjs, _ := utils.LoadPackageJSON(dir.Path)
	versions := utils.GetPublishedPackages(dir)
	for _, tag := range dir.Tags {
		jobs = append(jobs, models.Job{
			Tag:              *tag,
			Path:             dir.Path,
			PackageJSON:      dir.PackageJSON,
			Registry:         registry,
			Install:          install,
			NPMrc:            npmrc,
			Verbose:          verbose,
			Done:             false,
			Version:          pjs.Version,
			AlreadyPublished: versions,
		})
	}

	return jobs, nil
}

// CheckoutToHash takes a Job struct
func CheckoutToHash(job models.Job) error {
	r, err := git.PlainOpen(job.Path)
	if err != nil {
		return err
	}

	head, err := r.Head()
	if err != nil {
		return err
	}

	w, err := r.Worktree()
	if err != nil {
		return err
	}
	if job.Tag.Type() == plumbing.TagObject {
		commit, _ := job.Tag.Commit()
		if job.Verbose {
			fmt.Printf("Checking out to %s\n", commit.Hash.String())
		}
		err = w.Checkout(&git.CheckoutOptions{Hash: commit.Hash})
		if err != nil {
			if job.Verbose {
				fmt.Println("Resetting")
			}
			w.Reset(&git.ResetOptions{
				Mode:   git.HardReset,
				Commit: head.Hash(),
			})
			err = CheckoutToHash(job)
			if err != nil {
				return err
			}
		}

	}

	return nil
}
