// Package models provides ...
package models

import (
	"strings"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

// Dir is a thing and does a thing blah blah
type Dir struct {
	Path        string
	PackageJSON PackageJSON
	Repo        *git.Repository
	Tags        []*object.Tag
}

// Job contains a path and a hash this will be consumed
type Job struct {
	Path             string
	PackageJSON      PackageJSON
	Tag              object.Tag
	Done             bool
	Registry         string
	NPMrc            string
	Install          bool
	Verbose          bool
	Version          string
	AlreadyPublished []string
}

// PackageJSON bladasds das asd asd
type PackageJSON struct {
	Name    string  `json:"name"`
	Version string  `json:"version"`
	Scripts Scripts `json:"scripts"`
}

// Scripts is the Scripts block for PackageJSON
type Scripts struct {
	Build       string `json:"build"`
	Sass        string `json:"sass"`
	Start       string `json:"start"`
	Preversion  string `json:"preversion"`
	Version     string `json:"version"`
	Postversion string `json:"postversion"`
	Prepublish  string `json:"prepublish"`
}

// RequiresBuild checks if the package requires a build before publish
func (script *Scripts) RequiresBuild() bool {
	if script.Build != "" {
		return true
	}
	var containsBuild bool
	containsBuild = strings.Contains(script.Preversion, "build")
	containsBuild = strings.Contains(script.Postversion, "build")
	containsBuild = strings.Contains(script.Version, "build")
	containsBuild = strings.Contains(script.Prepublish, "build")
	return containsBuild

}
